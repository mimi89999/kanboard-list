#!/usr/bin/env python3

import config

from kanboard import Kanboard

from xtermcolor import colorize



kb = Kanboard('https://noc.staszic.waw.pl/k/jsonrpc.php', config.username, config.password)

tasks = kb.get_all_tasks(project_id=2)

for task in tasks:
    entry = "* " + task['title']# + "\n"

    if task['description']:
        entry += "\t--> " + task['description']# + "\n"

    if task['color']['background'][0] == '#':
        r = int(task['color']['background'][1:3], 16)
        g = int(task['color']['background'][3:5], 16)
        b = int(task['color']['background'][5:7], 16)
    else:
        rgb_color = task['color']['background'][4:-1].split(', ')

        r = int(rgb_color[0])
        g = int(rgb_color[1])
        b = int(rgb_color[2])

    encoded_color = int('%02x%02x%02x' % (r, g, b), 16)

    print(colorize(entry, rgb=encoded_color))
